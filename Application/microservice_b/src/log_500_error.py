# Script to simulate a HTTP Client , log only 500 Internal Server Error
# Version : 1.0
# DOC : Aug 25 2021
# Author : Lokesh Lingaiah


import requests
import logging
import os

print ("This is the client requesting for page from microservice_a")
response = requests.get(os.environ.get('MS_A_LINK'))

# Log the response to log file and print to stdout
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)-5.5s] %(message)s",
    encoding='utf-8',
    handlers=[
        logging.FileHandler("/var/log/AppError.log"),
        logging.StreamHandler()
    ]
)

# Log only if 500 Internal Server Error
if response.status_code == 500 :
    logging.error(response.content)
    logging.info(response.headers)

