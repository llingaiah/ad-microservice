# Script to simulate a HTTP Server , respond back with 200 OK or 500 Error randomly
# Version : 1.0
# DOC : Aug 25 2021
# Author : Lokesh Lingaiah

from http.server import BaseHTTPRequestHandler, HTTPServer
import time

hostName = "127.0.0.1"
serverPort = 8080

# Response for 200 OK
def respond_200ok(self):
    self.send_response(200)
    self.send_header("Content-type", "text/html")
    self.end_headers()
    self.wfile.write(bytes("<p>Request: %s</p>" % self.path, "utf-8"))
    self.wfile.write(bytes("<body>", "utf-8"))
    self.wfile.write(bytes("<p>This is output of random response service with 200 OK response.</p>", "utf-8"))
    self.wfile.write(bytes("</body></html>", "utf-8"))
    return

# Response for 500 Internal Server Error
def respond_500error(self):
    self.send_response(500)
    self.send_header("Content-type", "text/html")
    self.end_headers()
    self.wfile.write(bytes("<p>Request: %s</p>" % self.path, "utf-8"))
    self.wfile.write(bytes("<body>", "utf-8"))
    self.wfile.write(bytes("<p>This is output of random response Internal Server Error 500 Error response.</p>", "utf-8"))
    self.wfile.write(bytes("</body></html>", "utf-8"))
    return


# Request Handler
class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        call_func = [respond_200ok , respond_500error]
        from random import choice
        choice (call_func)(self)


# Start of Script fucntionality
if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
