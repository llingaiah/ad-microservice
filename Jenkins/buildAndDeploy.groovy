buildPath = "${env.JOB_NAME}/${env.BUILD_NUMBER}"
extWorkspace = exwsAllocate diskPoolId: 'gitDiskPool', path: "${customPath}"

node {

        stage('Check out microservice-a code, build docker image and push to docker hub')
        {
                node(dockerbuildmachine)
                {
                        try {
                                  exws (extWorkspace)
                              dir("${CWD}/app/")
                              def chkPoint = checkout ([$class: 'GitSCM',
                                                 branches: [[name: 'master']],
                                                 doGenerateSubmoduleConfigurations: false,
                                                         extensions: [[$class: 'CloneOption', reference: '', shallow: false, timeout: 200]],
                                                     submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'jenkins', url: "ssh://git@bitbucket.org/llingaiah/ad-microservice.git"]]])

                        # Record the build number in stash log

                if ( chkPoint.GIT_PREVIOUS_SUCCESSFUL_COMMIT )
                {
                    def gitFwbuildCommitMsg = sh (script: "git log -1 --oneline", returnStdout: true)
                                      gitFwbuildCommitMsg = gitFwbuildCommitMsg.trim()
                                      echo "gitFwbuildCommitMsg: ${gitFwbuildCommitMsg}"
                }

                    # Build docker image from DockerFile for microservice_a

                        sh "cd ${CWD}/app/Application/microservice_a ; docker build -t lokeshlingaiah/microservice_a:${env.BUILD_NUMBER} ."

                    # Build docker image from DockerFile for microservice_b

                        sh "cd ${CWD}/app/Application/microservice_b ; docker build -t lokeshlingaiah/microservice_b:${env.BUILD_NUMBER} ."

                        # Push the image to docker hub

                        sh "docker image push lokeshlingaiah/microservice_a:${env.BUILD_NUMBER}"
                        sh "docker image push lokeshlingaiah/microservice_b:${env.BUILD_NUMBER}"

            } catch (err) {
                                echo err.toString()
                                echo err.getMessage()
                                exws (extWorkspace) {
                                        sh "echo FAILURE in Checkout of code: ${pwd()}"
                                }
                                currentBuild.result = 'FAILURE'

                                mail body: "Git clone failed: ${env.BUILD_URL}" ,
                                from: 'buildadmin@microservice.com',
                        replyTo: 'buildadmin@microservice.com',
                                subject: 'CI/CD Git clone failed',
                                to: 'stakeholders@microservice.com'
                        }
                throw err
            }
    }

        stage('Deploy rollout on kubernetes') {

                node(kubeconsole) {
                sh "kubectl rollout status deployment webservice-a"
                def serviceALink = sh(script: "minikube service webservice-a --url", returnStdout: true)
                sh "sed -ri 's/REPLACE_THIS_LINK_ONGO/${serviceALink}/' /var/app/minikube/microservice_b/microservice-b-scheduler.yaml"
                sh "kubectl apply -f /var/app/minikube/microservice_b/microservice-b-scheduler.yaml"
                }
    }

}